# JSON to Datatype

Simple function which takes a JSON object or JSON-formatted string and returns an object with sensible datatypes.

Includes type definitions.

## Installation

`npm install json-to-datatype`

## Usage

Javascript

```js
const jsonToDatatype = require('json-to-datatype').jsonToDatatype;
```

Typescript

```ts
import { jsonToDatatype } from 'json-to-datatype';
```

## Conversions

+ Numbers  
+ Booleans  
+ Dates

## Links

[npm](https://www.npmjs.com/package/json-to-datatype)