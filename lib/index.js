/**
 * @file Main file (JavaScript)
 * @module json-to-datatype
 * @author Mike Doughty <mike@saverio.design>
 */

// use moment
let moment = require('moment');

/** 
 * Convert a JSON object or JSON-formatted String into a properly-
 * typed JavaScript object by parsing Numbers, Booleans, and Dates
 * from JSON String values.
 * @arg { any } obj
 * @return { any }
 */
module.exports.jsonToDatatype = (obj) => {

    let newObj;
    
    // parse JSON
    if ( typeof( obj ) === 'string' ) {
        try {
            obj = JSON.parse( obj );
        } catch ( e ) {
            console.error( e );
            return null;
        }
    }
    
    newObj = obj;

    for ( let i in obj ) {

        // recursive through object tree
        if ( obj[i] !== null && typeof ( obj[i] ) === 'object' ) newObj[i] = exports.jsonToDatatype( obj[i] );
        
        else if ( obj[i] !== null && typeof ( obj[i] ) === 'string' ) {

            // Number
            if ( !isNaN( obj[i] ) ) newObj[i] = obj[i] * 1;
            // Boolean
            else if ( obj[i] === 'true' ) newObj[i] = true;
            else if ( obj[i] === 'false' ) newObj[i] = false;
            // Date
            else if ( moment( obj[i], moment.ISO_8601, true ).isValid() ) {
                let m = moment( obj[i] );
                newObj[i] = m.toDate();
            };

        }

    }

    return newObj;

};
