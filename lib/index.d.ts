declare namespace jsonToDatatype {
    /** 
     * Convert a JSON object or JSON-formatted String into a properly-
     * typed JavaScript object by parsing Numbers, Booleans, and Dates
     * from JSON String values.
 * @arg { any } obj
 * @return { any }
     */
    export function jsonToDatatype (obj : any) : any;
}

export = jsonToDatatype;
